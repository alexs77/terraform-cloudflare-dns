variable "zone_name" {
  type        = string
  description = "Name of to be modified zone. Needs to exist."
}

variable "records" {
  # The keys of the variable lists define the record type.
  # Available values: A, AAAA, CAA, CNAME, TXT, SRV, LOC, MX, NS, SPF, CERT, DNSKEY, DS, NAPTR, SMIMEA, SSHFP, TLSA, URI, PTR, HTTPS, SVCB.
  type = map(list(object({
    name = string

    allow_overwrite = optional(bool, false)
    comment         = optional(string, "Managed by Terraform. Manual changes might be overwritten!")
    priority        = optional(number)
    proxied         = optional(bool)
    tags            = optional(set(string))
    ttl             = optional(number)

    # Either use "content" attribute or "data" block, depending on "type"
    content = optional(string)

    data = optional(object({
      value = optional(string)

      algorithm      = optional(number)
      altitude       = optional(number)
      certificate    = optional(string)
      content        = optional(string)
      digest         = optional(string)
      digest_type    = optional(number)
      fingerprint    = optional(string)
      flags          = optional(string)
      key_tag        = optional(number)
      lat_degrees    = optional(number)
      lat_direction  = optional(string)
      lat_minutes    = optional(number)
      lat_seconds    = optional(number)
      long_degrees   = optional(number)
      long_direction = optional(string)
      long_minutes   = optional(number)
      long_seconds   = optional(number)
      matching_type  = optional(number)
      name           = optional(string)
      order          = optional(number)
      port           = optional(number)
      precision_horz = optional(number)
      precision_vert = optional(number)
      preference     = optional(number)
      priority       = optional(number)
      proto          = optional(string)
      protocol       = optional(number)
      public_key     = optional(string)
      regex          = optional(string)
      replacement    = optional(string)
      selector       = optional(number)
      service        = optional(string)
      size           = optional(number)
      tag            = optional(string)
      target         = optional(string)
      type           = optional(number)
      usage          = optional(number)
      weight         = optional(number)
    }))

    timeouts = optional(object({
      create = optional(string)
      update = optional(string)
    }))

  })))
}
