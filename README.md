# Terraform Cloudflare DNS

Configure DNS records on Cloudflare. The zones must already exist on
Cloudflare.

## Table of Contents

- [Terraform Cloudflare DNS](#terraform-cloudflare-dns)
  - [Table of Contents](#table-of-contents)
  - [Usage](#usage)
    - [Inclusion of module](#inclusion-of-module)
    - [Variables](#variables)

## Usage

To configure DNS on Cloudflare, the [Cloudflare
Provider](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs)
is used. An API token is required. To be setup on [Cloudflare
Dashboard => Profile => API
Tokens](https://dash.cloudflare.com/profile/api-tokens).

### Inclusion of module

Add these lines to your Terraform source code:

```hcl
terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.39"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

module "zone_zonename" {
  source = "https://gitlab.com/alexs77/terraform-cloudflare-dns.git"

  records   = var.records
  zone_name = var.zone_name
}
```

### Variables

In a file like `zonename.tfvars`, define:

```hcl
zone_name = "example.net"

records = {
    "TXT" = [
        {
            name  = "foo"
            value = "bar"
        },
        {
            name  = "example"
            value = "\"v=spf1 mx a -all\""
            comment = "An SPF record. Needs to be quoted"
        },        
    ],

  "SRV" = [
    {
      allow_overwrite = false
      comment         = "An SRV record. Needs to have a data block. NO value attribute set."
      data = {
        service  = "_sip"
        proto    = "_tls"
        name     = "3records.example.com"
        priority = 34321
        weight   = 35432
        port     = 36543
        target   = "example.net"
      }
      name     = "exampleterraformsrv.5y5.one"
      priority = 1
      proxied  = false
      tags     = ["value"]
      timeouts = {
        create = "value"
        update = "value"
      }
      ttl = 100
    },
  ],
}

# Complete list
records = {
  "Record Type" = [{
    allow_overwrite = false
    comment = "value"
    data = {
      algorithm = 1
      altitude = 1
      certificate = "value"
      content = "value"
      digest = "value"
      digest_type = 1
      fingerprint = "value"
      flags = "value"
      key_tag = 1
      lat_degrees = 1
      lat_direction = "value"
      lat_minutes = 1
      lat_seconds = 1
      long_degrees = 1
      long_direction = "value"
      long_minutes = 1
      long_seconds = 1
      matching_type = 1
      name = "value"
      order = 1
      port = 1
      precision_horz = 1
      precision_vert = 1
      preference = 1
      priority = 1
      proto = "value"
      protocol = 1
      public_key = "value"
      regex = "value"
      replacement = "value"
      selector = 1
      service = "value"
      size = 1
      tag = "value"
      target = "value"
      type = 1
      usage = 1
      value = "value"
      weight = 1
    }
    name = "value"
    priority = 1
    proxied = false
    tags = [ "value" ]
    timeouts = {
      create = "value"
      update = "value"
    }
    ttl = 1
    value = "value"
  }]
}
```
