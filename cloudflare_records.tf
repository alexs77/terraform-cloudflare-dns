resource "cloudflare_record" "these" {
  for_each = tomap({
    for obj in flatten([
      for key, value in var.records : [
        for item in value : {
          type = key

          name    = "${item.name},${key},${item.data != null ? base64encode(jsonencode(item.data)) : item.content}"
          content = item.content

          allow_overwrite = item.allow_overwrite
          comment         = item.comment
          priority        = item.priority
          proxied         = item.proxied
          tags            = item.tags
          ttl             = item.ttl
          timeouts        = item.timeouts

          data = item.data
        }
      ]
    ]) : obj.name => obj
  })

  name    = split(",", each.value.name)[0]
  type    = each.value.type
  zone_id = data.cloudflare_zone.this.id

  allow_overwrite = each.value.allow_overwrite
  comment         = each.value.comment
  priority        = each.value.priority
  proxied         = each.value.proxied
  tags            = each.value.tags
  ttl             = each.value.ttl

  # Either use "content" attribute or "data" block, depending on "type"
  content = each.value.content

  dynamic "data" {
    for_each = each.value.data == null ? [] : [1]

    content {
      algorithm      = each.value.data.algorithm
      altitude       = each.value.data.altitude
      certificate    = each.value.data.certificate
      content        = each.value.data.content
      digest         = each.value.data.digest
      digest_type    = each.value.data.digest_type
      fingerprint    = each.value.data.fingerprint
      flags          = each.value.data.flags
      key_tag        = each.value.data.key_tag
      lat_degrees    = each.value.data.lat_degrees
      lat_direction  = each.value.data.lat_direction
      lat_minutes    = each.value.data.lat_minutes
      lat_seconds    = each.value.data.lat_seconds
      long_degrees   = each.value.data.long_degrees
      long_direction = each.value.data.long_direction
      long_minutes   = each.value.data.long_minutes
      long_seconds   = each.value.data.long_seconds
      matching_type  = each.value.data.matching_type
      name           = each.value.data.name
      order          = each.value.data.order
      port           = each.value.data.port
      precision_horz = each.value.data.precision_horz
      precision_vert = each.value.data.precision_vert
      preference     = each.value.data.preference
      priority       = each.value.data.priority
      proto          = each.value.data.proto
      protocol       = each.value.data.protocol
      public_key     = each.value.data.public_key
      regex          = each.value.data.regex
      replacement    = each.value.data.replacement
      selector       = each.value.data.selector
      service        = each.value.data.service
      size           = each.value.data.size
      tag            = each.value.data.tag
      target         = each.value.data.target
      type           = each.value.data.type
      usage          = each.value.data.usage
      value          = each.value.data.value        # needs to be replaced with "content" as well?
      weight         = each.value.data.weight
    }
  }
}
